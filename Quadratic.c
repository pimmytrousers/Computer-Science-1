#include <stdio.h>
#include <math.h>
#include <stdlib.h>

// DONT FORGET THE -LM
int main()
{
  double a, b, c, d, root1, root2, iroot1, iroot2, q ;
  printf("give the coefficients of (a*x^2)+(b*x)+c in the order a,b,c:\n") ;
  
  scanf("%lf %lf %lf", &a, &b, &c) ;
  
  d = b*b-4*a*c ; // the square root part
  
  if (a == 0) {
    printf("nothing works\n") ;
  }
  
  else if (d>0) {
    root1 = (-b + sqrt(d)) / (2*a);
    root2 = (-b - sqrt(d)) / (2*a);
    
    printf("the roots of your equation are %lf and %lf\n", root1, root2) ; 
    
    
  }
  else if (d<0) {
    
    d= -d;
    iroot1 = (-b/2*a) ;
    iroot2 = (sqrt(d)/2*a) ;
    
    printf("the unreal root is %lf+%.lfi and %lf-%.lfi\n",iroot1, iroot2, iroot1, iroot2) ;
    
  }
}



   

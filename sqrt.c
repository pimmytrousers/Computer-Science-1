#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main()
{
  double x,y ;
  printf("enter a number : ") ; //what happens if you put a negative?
  scanf("%lf",&x) ;
  if (x < 0) {
    printf("There is no real answer to the sqrt(%lf)/n",x) ;
  } else {
  y = sqrt(x) ;
  printf("The square root of %lf is %lf", x, y) ;
  }
}

#include <stdio.h>
int main()
{
  double a, b, m, x; // the first numb1er is a the second is b and m is the answer
  
  printf("Enter two positive integers: ");
  
  scanf("%lf %lf", &a, &b);
  
  m=(a>b) ? a : b; /* maximum value is stored in variable m */
  
  while(1)                       /* Always true. */
  {
      if(a==0 && b==0)
      {
          
        printf("LCM of %lf and %lf is %lf\n", a, b, m);
        
        break;          /* while loop terminates. */
      }
      m = m + 1;
  }
  return 0;
}
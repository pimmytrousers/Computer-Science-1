#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main(){

	double n, i, p, r, x;

	printf("Enter the amount of products you want: ");
	scanf("%lf", &n);

	p = 1;
	r = 1;

	for(i=2;i+1<n+2;i++){
		if(fmod(i,2)==0){
			r=r-1/i;
		}
		if(fmod(i,2)!=0){
			r=r+1/i;
		}

		p=p*r;
	}
	printf("The answer is %lf\n", p);
}
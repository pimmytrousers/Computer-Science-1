#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/bn.h>
#include <gmp.h>

int modInverse(int e, int pn){
  e = e % pn;
  for(int d = 1; d < pn; d++) {
    if((e*d) % pn == 1) return d;
    }
    return 0;
}

int hcf(int a, int h) 
{
    int temp; 
    while(1)
    {
        temp = a%h;
        if(temp==0)
        return h;
        a = h;
        h = temp;
    }
} 

int main(){
  long long p, q, n, d, pn, e, x[1000], y[1000], c[1000], Var, gcd, gcd1, Var2, flag;
  char z[1000], vz[1000];
  int i;
  
  printf("Input two prime numbers and a coprime less than (n*p):\n");
  scanf("%llu %llu %llu", &p, &q, &e);
  
  n = (q*p);
  pn = (q-1)*(p-1);

  gcd = hcf(p,e);

  gcd1 = hcf(q,e);

  for(Var2=1;Var2*2<=p;Var2++){
      if(p % Var2==0){
        Var++;
      }
    }
    if(Var>1){
      printf("\nTry a different number for p\n\n");
    }
    else{
      flag++;
    }
    
    Var = 0;
    
    for(Var2=1;Var2*2<=q;Var2++){
      if(q % Var2==0){
        Var++;
      }
    }
    if(Var>1){
      printf("Try a different number for q\n\n");
    }
    else{
      flag++;
    }

    Var = 0;

    for(Var2=1;Var2*2<=e;Var2++){
      if(e % Var2==0){
        Var++;
      }
    }
    if(Var>1){
    printf("Try a different number for e\n\n");
    }
    else{
      flag++;
    }

    if(flag==3){
      if(gcd && gcd1==1){

  printf("\nThe public key is (%.llu, %.llu)\n", e, n);

  d = modInverse(e, pn);

  printf("the private key is (%.llu, %.llu)\n\n", d, n);

  printf("enter a message to decrypt: \n\n");
  scanf("%s",z);

  for(i=0;i<strlen(z);i++){

        if (z[i]>='a' && z[i]<='o'){
            
            vz[i] = z[i]-'a'+2;
        }
        if (z[i]=='p'){
            
            vz[i] = z[i]-'a'+3;
        }
        if (z[i]=='q'){
            
            vz[i] = z[i]-'a'+4;
        }
        if (z[i]=='r'){
            
            vz[i] = z[i]-'a'+5;
        }
        if (z[i]=='s'){
            
            vz[i] = z[i]-'a'+6;
        }
        if (z[i]=='t'){
            
            vz[i] = z[i]-'a'+7;
        }
        if (z[i]=='u'){
            
            vz[i] = z[i]-'a'+8;
        }
        if (z[i]=='v'){
            
            vz[i] = z[i]-'a'+9;
        }
        if (z[i]=='w'){
            
            vz[i] = z[i]-'a'+10;
        }
        if (z[i]=='x'){
            
            vz[i] = z[i]-'a'+11;
        }
        if (z[i]=='y'){
            
            vz[i] = z[i]-'a'+12;
        }
        if (z[i]=='z'){
            
            vz[i] = z[i]-'a'+13;
        }
        if (z[i]=='_'){
            
            vz[i] = z[i]-'_'+40;
        }
         printf("%d\n\n",vz[i]);
    }
    

for (i=0;i<strlen(z);i++){
  
  c[i] = fmod((pow(vz[i],e)),n);

  printf("the ciphertext is %.llu \n\n", c[i]);
}
  
  for (i=0;i<strlen(z);i++){

    x[i] = 1;

    d = modInverse(e, pn);

    while(d>0){

    if(fmod(d,2) == 1){
      x[i] = (x[i]*c[i]);
      
      if (x[i]>n) {
        x[i] = fmod(x[i],n);
      }
    }

    c[i] = (c[i]*c[i]);
    
    if (c[i]>n) {
      c[i] = fmod(c[i],n);
    }
    d=floor(d/2);

}
  printf("the plaintext of x[%d] is %.llu \n\n", i, x[i]);
}

for(i=0;i<strlen(z);i++){

        if(x[i]>=2 && x[i]<=16){
            
            y[i] = x[i]+95;
        }
        if(x[i]==18){
            
            y[i] = x[i]+94;
        }
        if(x[i]==20){
            
            y[i] = x[i]+93;
        }
        if(x[i]==22){
            
            y[i] = x[i]+92;
        }
        if(x[i]==24){
            
            y[i] = x[i]+91;
        }
        if(x[i]==26){
            
            y[i] = x[i]+90;
        }
        if(x[i]==28){
            
            y[i] = x[i]+89;
        }
        if(x[i]==30){
            
            y[i] = x[i]+88;
        }
        if(x[i]==32){
            
            y[i] = x[i]+87;
        }
        if(x[i]==34){
            
            y[i] = x[i]+86;
        }
        if(x[i]==36){
            
            y[i] = x[i]+85;
        }
        if(x[i]==38){
            
            y[i] = x[i]+84;
        }

        if(x[i]==40){
          y[i] = x[i]+55;
        }

         int a = y[i];
        char c = a;
        printf("%c", c);
    }
    printf("\n\n");
}

}

if(flag!=3){
  printf("One or more of the numbers chosen is not prime\n\n");
}

}

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/bn.h>

int modInverse(int e, int pn){
  e = e % pn;
  for(int d = 1; d < pn; d++) {
    if((e*d) % pn == 1) return d;
    }
    return 0;
}

int main(){
  long long prime1, prime2, product, m[1000], d, pn, coprime, x[1000], y[1000], c[1000], dprime1, dprime2, qinv, blam, m1, m2, h;
  char z[1000], vz[1000];
  int i;

  fflush(stdin);
  fflush(stdout);
  
  printf("Input two prime numbers and a coprime less than (n*p):\n");
  scanf("%llu %llu %llu", &prime1, &prime2, &coprime);
  
  product = (prime1*prime2);
  pn = (prime2-1)*(prime1-1);

  printf("\nThe public key is (e=%.llu, n=%.llu)\n", coprime, product);

  d = modInverse(coprime, pn);

  printf("the private key is (d=%.llu, n=%.llu)\n\n", d, product);

  printf("enter a message to decrypt: \n\n");
  scanf("%s",z);

  for(i=0;i<strlen(z);i++){
      switch(z[i]){

        if (z[i]>='a' && z[i]<='o'){
            
            vz[i] = z[i]-'a'+2;
        }
        case 'p':       
            vz[i] = z[i]-'a'+3;
            break;
        case 'q':
            vz[i] = z[i]-'a'+4;
            break;
        case 'r':
            vz[i] = z[i]-'a'+5;
            break;
        case 's':
            vz[i] = z[i]-'a'+6;
            break;
        case 't':
            vz[i] = z[i]-'a'+7;
            break;
        case 'u':
            vz[i] = z[i]-'a'+8;
            break;
        case 'v':
            vz[i] = z[i]-'a'+9;
            break;
        case 'w':
            vz[i] = z[i]-'a'+10;
            break;
        case 'x':
            vz[i] = z[i]-'a'+11;
            break;
        case 'y':
            vz[i] = z[i]-'a'+12;
            break;
        case 'z':      
            vz[i] = z[i]-'a'+13;
            break;
        case '_':   
            vz[i] = z[i]-'_'+40;
            break;
          }
         printf("%d\n\n",vz[i]);
    }

    dprime1 = d % (prime1-1);

    dprime2 = d % (prime2-1);

    qinv =modInverse(prime2,prime1);

    blam = (qinv*prime2) % prime1;

for (i=0;i<strlen(z);i++){
  
  c[i] = fmod((pow(vz[i],coprime)),product);

  printf("the ciphertext is %.llu \n\n", c[i]);
}
  
  for (i=0;i<strlen(z);i++){
    
    m1 = fmod((pow(c[i],dprime1)),prime1);

    m2 = fmod((pow(c[i],dprime2)),prime2);

    h = fmod((qinv*(m1-m2)),prime1);

    m[i]= m2+(h*prime2);

  printf("the plaintext of x[%d] is %.llu \n\n", i, m[i]);
}

for(i=0;i<strlen(z);i++){

        if(x[i]>=2 && x[i]<=16){
            
            y[i] = x[i]+95;
        }
        if(x[i]==18){
            
            y[i] = x[i]+94;
        }
        if(x[i]==20){
            
            y[i] = x[i]+93;
        }
        if(x[i]==22){
            
            y[i] = x[i]+92;
        }
        if(x[i]==24){
            
            y[i] = x[i]+91;
        }
        if(x[i]==26){
            
            y[i] = x[i]+90;
        }
        if(x[i]==28){
            
            y[i] = x[i]+89;
        }
        if(x[i]==30){
            
            y[i] = x[i]+88;
        }
        if(x[i]==32){
            
            y[i] = x[i]+87;
        }
        if(x[i]==34){
            
            y[i] = x[i]+86;
        }
        if(x[i]==36){
            
            y[i] = x[i]+85;
        }
        if(x[i]==38){
            
            y[i] = x[i]+84;
        }

        if(x[i]==40){
          y[i] = x[i] +55;
        }
      }

         int a = y[i];
        printf("%c", a);
  
    printf("\n\n");

    printf("m1 is %llu, m2 is %llu, h is %llu\n\n", m1, m2, h);

}
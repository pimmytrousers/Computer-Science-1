#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

int modInverse(int a, int m){
  a = a % m;
  for(int x = 1; x < m; x++) {
    if((a*x) % m == 1) return x;
    }
    return 0;
}

int main(){
  long long p, q, n, m, d, pn, e, x[1000], y[1000], c[1000];
  char z[1000], vz[1000];
  int i;
  

  fflush(stdin);
  fflush(stdout);
  
  printf("Input two prime numbers and a coprime less than (n*p):\n") ;
  scanf("%llu %llu %llu", &p, &q, &e);
  
  n = (q*p);
  pn = (q-1)*(p-1);

  printf("\nThe public key is (%.llu, %.llu)\n", e, n);

  d = modInverse(e, pn);

  printf("the private key is (%.llu, %.llu)\n\n", d, n);

  printf("enter a message to decrypt: \n");
  scanf("%s",z);

  for(i=0;i<strlen(z);i++){

        if(z[i]>='a' && z[i]<='z'){
            
            vz[i] = z[i]-'a'+2;
        }
    }

for (i=0;i<strlen(z);i++){
  
  c[i] = fmod((pow(vz[i],e)),n);

  printf("the ciphertext is %.llu \n\n", c[i]);
}

  for (i=0;i<strlen(z);i++){

    x[i] = 1;

    d = modInverse(e, pn);

    while(d>0){
    
    if(fmod(d,2) == 1){
      x[i] = (x[i]*c[i]);
      
      if (x[i]>n) {
        x[i] = fmod(x[i],n);
      }
    }

    c[i] = (c[i]*c[i]);
    
    if (c[i]>n) {
      c[i] = fmod(c[i],n);
    }
    d=d/2;

  }
}

for(i=0;i<strlen(z);i++){

        if(x[i]>=2 && x[i]<=28){
            
            y[i] = x[i]+95;
        }

         int a = y[i];
        char c = a;
        printf("%c\n", c);
    }


}
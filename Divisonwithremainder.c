#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main()
{
  double  a, b, x, y, p, r;
  
  printf("Input a base number and the divisor: ") ;
  	fflush(stdin);
  	fflush(stdout);
  scanf("%lf %lf", &b, &a);

  x = 0;

  	while(b>=a){

  		b = b - a; 
  		x = x + 1;
  	}
  	printf("the remainder is %lf and the quotient is %lf\n", b, x);
  }
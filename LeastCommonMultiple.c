#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main()
{
    double a, b, a1, b1, m; // a is the first n umber b is te second and a1 and b1 are temporary values for the equation to work m is the gcd of a and b
   
    printf("Enter two integers: ");
   
    scanf("%lf %lf", &a, &b);
    a1 = a;
    b1 = b;
    
    while(a1!=b1){

        if(a1>b1){
           
            a1 = a1 - b1;
        }
        else
           
            b1 = b1 - a1;
    }

    m = (a*b)/a1;

    printf("LCM of %.lf and %.lf is %.lf\n", a, b, m); // the equation for LCM(a,b)= (a*b)/(GCD(a,b))
 
}

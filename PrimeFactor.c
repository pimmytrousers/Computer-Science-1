#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main() {
    long long n;
    int i;
    n = 0;
    printf("what is the value of n: \n");
    scanf("%llu", &n);
    while (n % 2 == 0){ //step 1
        printf("%d ", 2);
        n = n/2;
    }
 
   
    for (i=3;i<=sqrt(n);i=i+2){//step 2
       while (n%i == 0){
            n=n/i;
        }
    }
 
    if (n>2){
        printf ("%llu is the biggest prime factor\n\n", n);
    }
}
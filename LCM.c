#include <stdio.h>
int main()
{
  int a, b, m; // the first numb1er is a the second is b and m is the answer
  
  printf("Enter two positive integers: ");
  
  scanf("%d %d", &a, &b);
  
  m=(a>b) ? a : b; /* maximum value is stored in variable m */
  
  while(1)                       /* Always true. */
  {
      if(m%a==0 && m%b==0)
      {
          
        printf("LCM of %d and %d is %d\n", a, b, m);
        
        break;          /* while loop terminates. */
      }
      m = m + 1;
  }
  return 0;
}
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main()
{
  double b, c, x ;
  
  printf("solving b=x+c=0, Input b and c: ") ;

  scanf("%lf %lf", &b, &c) ;
  
  if (b!=0) {
    x=-c/b;
    printf("%lf", x) ;
    }
  else {
    if (c==0) {
      printf("everything works\n") ;
	}
    else {
      printf("nothing works\n") ;
    }
  }
}

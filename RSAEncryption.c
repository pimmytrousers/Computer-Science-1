#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <gmp.h>
#include <assert.h>

int modInverse(int e, int pn){
  e = e % pn;
  for(int d=1;d<pn;d++) {
    if((e*d) % pn == 1) return d;
    }
    return 0;
}

int hcf(int a, int h) 
{
    int temp; 
    while(1)
    {
        temp = a%h;
        if(temp==0)
        return h;
        a = h;
        h = temp;
    }
} 

int main(){
  signed long int p, q, n, d, pn, e, x[1025], C[1025], Var, gcd, gcd1, Var2, flag, plain[1025];
  char z[1025], vz[1025], c[1025];
  int i, Flag, j, k;
  mpz_t big[1025];
  
  printf("Input two prime numbers and a coprime less than (n*p):\n");
  scanf("%li %li %li", &p, &q, &e);
  
  n = (q*p);
  pn = (q-1)*(p-1);

  gcd = hcf(p,e);
  gcd1 = hcf(q,e);

  for(Var2=1;Var2*2<=p;Var2++){ /* Primality check for p, q and e */
      if(p % Var2==0){
        Var++;
      }
    }
    if(Var>1){
      printf("\nTry a different number for p\n\n");
    }
    else{
      flag++;
    }
    
    Var = 0;
    
    for(Var2=1;Var2*2<=q;Var2++){
      if(q % Var2==0){
        Var++;
      }
    }
    if(Var>1){
      printf("Try a different number for q\n\n");
    }
    else{
      flag++;
    }

    Var = 0;

    for(Var2=1;Var2*2<=e;Var2++){
      if(e % Var2==0){
        Var++;
      }
    }
    if(Var>1){
    printf("Try a different number for e\n\n");
    }
    else{
      flag++;
    }

    if(flag==3){
      if(gcd && gcd1==1){

  printf("\nThe public key is (%.li, %.li)\n", e, n);

  d = modInverse(e, pn);

  printf("the private key is (%.li, %.li)\n\n", d, n);

  printf("enter a message to decrypt: \n\n");
  scanf("%1024s",z);

  for(i=0;i<strlen(z);i++){ /*converts the letters into numbers so they can be turned into encryption codes*/
     for(j=0;j<57;j++){
        
        if(z[i]=='A'+j){
          c[i]=65+j;
        }
      }
    }
    for(k=0;k<strlen(z);k++){
      printf("%i\n", c[k]);
    }
for(i=0;i<strlen(z);i++){
  C[i]=big[i];
}
for (i=0;i<strlen(z);i++){

  C[i] = fmod((pow(c[i],e)),n); /* creates the ciphertext */

  printf("the ciphertext is %.li \n\n", C[i]);
}

for (i=0;i<strlen(z);i++){ /* the math to convert the ciphertext into plaintext again */
    
    plain[i] = fmod((pow(C[i],d)),n);

    printf("%li\n\n", plain[i]);
}    
    
    for(i=0;i<strlen(z);i++){


    }

if(flag!=3){
  printf("One or more of the numbers chosen is not prime\n\n");
}

}
}
}

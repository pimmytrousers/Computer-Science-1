#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

int main(){
	int i, j, k, r, c, flag, flag1, s;
	double x[100][100], y[100][100];

	scanf("%d", &s);
	printf("The size is %d\n", s);

	for(r=0;r<s;r++){//scans the original matrix
		for(c=0;c<s;c++){
			scanf("%lf", &x[r][c]);
		}
	}
	for(r=0;r<s;r++){//prints the first matrix
		printf("\n");
		for(c=0;c<s;c++){
			if(x[r][c]==1){
				flag++;
				printf("1");
			}
			else{
				printf("0");
		}
		}
	}
		printf("\n");
while(i<10){//prints the multiple iterations
	sleep(1);
	for(r=0;r<s;r++){
		for(c=0;c<s;c++){
			flag1=0;
				if(r>0){
					if(x[r-1][c]==1){//top
						flag1++;
					}
				}
				if(r>0 && c>0){
					if(x[r-1][c-1]==1){//top left
						flag1++;
					}
				}
				if(c>0){
					if(x[r][c-1]==1){//left
						flag1++;
					}
				}
				if(r<s-1 && c>0){
					if(x[r+1][c-1]==1){//bottom left
						flag1++;
					}
				}
				if(r<s-1){
					if(x[r+1][c]==1){//bottom
						flag1++;
					}
				}
				if(c<s-1 && r<s-1){
					if(x[r+1][c+1]==1){//bottom right
						flag1++;
					}
				}
				if(c<s-1){
					if(x[r][c+1]==1){//right
						flag1++;
					}
				}
				if(r>0 && c<s-1){
					if(x[r-1][c+1]==1){//top right
						flag1++;
					}
				}
				
				if(x[r][c]==1){
					if(flag1==2 || flag1==3){//rule for a neighbor
					y[r][c]=1;
				}
				else if(flag1>3 || flag1<2){//rule for a neighbor
					y[r][c]=0;
				}
				}
				if(x[r][c]==0){//rule for a dead neighbor
					if(flag1==3){
						y[r][c]=1;
					}
				if(flag1!=3){//rule for a dead neighbor
					y[r][c]=0;
				}
			}
		}
	}
	for(r=0;r<s;r++){//prints the following matrices
		printf("\n");
		for(c=0;c<s;c++){
			if(y[r][c]==1){
				printf("*");
			}
			else{
				printf(" ");
			}
		}
	}
	printf("\n");
	i++;
	for(r=0;r<s;r++){//replaces the old matrix with the new one
		for(c=0;c<s;c++){
			x[r][c]=y[r][c];
		}
	}
}
}